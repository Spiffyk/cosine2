/*
 * Cosine - a game for training basic values of trigonometric functions
 * Copyright (C) 2019 Oto Šťáva
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

import katex from "katex"
import $ from "jquery"

import Game from "./game.js"

const SCREEN_RADIUS_MULTIPLIER = 0.8;
const CIRCLE_RADIUS_MULTIPLIER = 1.25;
const SIGNAL_TIMEOUT_INCORRECT = 2000;
const SIGNAL_TIMEOUT_CORRECT = 1000;

const CIRCLE_RADIUS_MULTIPLIER_RESULT =
    SCREEN_RADIUS_MULTIPLIER * CIRCLE_RADIUS_MULTIPLIER;

let signalizeTimeout;
let isBackgroundCircleShrunk = false;

let
    $_document,
    $_window,
    $_body,
    $_bkgCircle,
    $_gameScreen,
    $_splash,
    $_container,
    $_question,
    $_gameControls,
    $_unitCircle,
    $_unitAngle;

function shrinkBackgroundCircle(time, callback) {
    isBackgroundCircleShrunk = true;
    $_gameScreen.fadeOut(time * 0.25);
    $_bkgCircle
        .animate({
            "width": "0",
            "height": "0",
            "opacity": "0"
        }, time, callback);
}

function unshrinkBackgroundCircle(time, callback) {
    isBackgroundCircleShrunk = false;
    $_gameScreen.fadeIn(time * 1.25);
    let diameter = getBackgroundCircleDiameter();
    $_bkgCircle
        .animate({
            "width": diameter + "px",
            "height": diameter + "px",
            "opacity": "1"
        }, time, callback);
}

function getBackgroundCircleDiameter() {
    return Math.min(Math.min($_document.width(), $_document.height()) * CIRCLE_RADIUS_MULTIPLIER_RESULT, 700);
}

function setBackgroundCircleSize() {
    if (!isBackgroundCircleShrunk) {
        let diameter = getBackgroundCircleDiameter();
        $_bkgCircle
            .width(diameter)
            .height(diameter);
    }
}

function setGameScreenSize() {
    let width = Math.min(Math.min($_document.width(), $_document.height()) * SCREEN_RADIUS_MULTIPLIER, 500);
    let height = width * 1.2;

    $_unitCircle
        .width(width * 0.4)
        .height(height * 0.4);

    $_gameControls
        .width(width)
        .height(height * 0.15)
        .css("font-size", (height * 0.06) + "px");

    $_question
        .height(height * 0.25)
        .css("font-size", (height * 0.20) + "px");
}

function setSizes() {
    setBackgroundCircleSize();
    setGameScreenSize();
}

function clearAnswer() {
    $(document.body)
        .removeClass("answer")
        .removeClass("right")
        .removeClass("wrong");
}

function updateScreen() {
    let q = Game.getQuestion();

    $_gameControls.find(".button").each(function () {
        let ans = parseInt($(this).attr("data-ans"));
        katex.render(q["options"][ans], this);
        if (ans === q["answer"]) {
            $(this).addClass("actually");
        } else {
            $(this).removeClass("actually");
        }
    });

    if (q["angleHint"] === undefined || q["angleHint"] === null) {
        $_unitCircle.fadeOut(250);
    } else {
        $_unitAngle.css({
            "transform": "rotate(-" + q["angleHint"] + "deg)"
        });
        $_unitCircle.fadeIn(250);
    }

    $_question.each(function () {
        katex.render(q["question"], this);
    });
}

/**
 * Signalizes whether the answer was right or wrong.
 */
function signalAnswer(right) {
    clearTimeout(signalizeTimeout);
    clearAnswer();
    $(document.body).addClass("answer");

    if (right) {
        $(document.body).addClass("right");
    } else {
        $(document.body).addClass("wrong");
    }

    signalizeTimeout = setTimeout(() => {
        shrinkBackgroundCircle(250, () => {
            updateScreen();
            unshrinkBackgroundCircle(250);
        });
        clearAnswer();
    }, (right) ? SIGNAL_TIMEOUT_CORRECT : SIGNAL_TIMEOUT_INCORRECT);
}

/**
 * Initializes the GUI.
 */
function init() {
    Game.init();

    $_document = $(document);
    $_window = $(window);
    $_body = $(document.body);
    $_bkgCircle = $(".background-circle");
    $_gameScreen = $(".game-screen");
    $_splash = $(".splash");
    $_container = $(".container");
    $_question = $(".question");
    $_gameControls = $(".game-controls");
    $_unitCircle = $(".unit-circle-svg");
    $_unitAngle = $_unitCircle.find(".unit-angle-line");

    $_gameControls.find(".button")
        .click(function () {
            signalAnswer(Game.checkAnswer(parseInt($(this).attr("data-ans"))))
        })
        .children();

    let lastTouchTime = Date.now();
    $_body.on("mousemove", function () {
        if (Date.now() - lastTouchTime < 500) {
            return;
        }
        $_body.addClass("hoverEnabled");
    });
    $_body.on("touchstart", function () {
        lastTouchTime = Date.now();
        $_body.removeClass("hoverEnabled");
    });

    $_body.on("mousedown", "*", function () {
        $(this).addClass("active")
    });
    $_body.on("mouseup", "*", function () {
        $(this).removeClass("active")
    });

    updateScreen();
    $_window.resize(setSizes);
    setSizes();

    shrinkBackgroundCircle();
    $_gameScreen.hide();
    $_splash.fadeOut(750, () => {
        $_container.show(() => {
            unshrinkBackgroundCircle(500, () => {
                $(".footer")
                    .mouseenter(() => {
                        shrinkBackgroundCircle(750);
                        $_splash.fadeIn(500);
                    })
                    .mouseleave(() => {
                        unshrinkBackgroundCircle(750);
                        $_splash.fadeOut(500);
                    });
            });
        });
    });
}

let Gui = {
    init: init
};

export default Gui;
