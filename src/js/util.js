/*
 * Cosine - a game for training basic values of trigonometric functions
 * Copyright (C) 2019 Oto Šťáva
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Gets a random element of the specified array.
 *
 * @param array {Array} the array to pick from
 * @returns {*} a random element of the array
 */
export function randomOf(array) {
    return array[randomIndexOf(array)];
}

/**
 * Gets a random index of the specified array.
 *
 * @param array {Array} the array to pick from
 * @returns {Number} a random element of the array
 */
export function randomIndexOf(array) {
    return Math.floor(Math.random() * array.length);
}