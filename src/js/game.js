/*
 * Cosine - a game for training basic values of trigonometric functions
 * Copyright (C) 2019 Oto Šťáva
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import {randomIndexOf, randomOf} from "./util";

const QUESTIONS_FILE = "res/questions.json";

let enabledSets = [ "trigon_deg", "trigon_rad" ];
let questionSets = null;
let currentQuestion = null;

function loadQuestions() {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", QUESTIONS_FILE, false);
    xhr.send(null);
    questionSets = JSON.parse(xhr.responseText);

    for (let setId in questionSets) {
        if (!questionSets.hasOwnProperty(setId)) {
            continue;
        }

        let set = questionSets[setId];
        let possibleAnswersMap = {};

        for (let q of set["questions"]) {
            for (let a of q["answers"]) {
                possibleAnswersMap[a] = true;
            }
        }

        let possibleAnswers = [];
        for (let answer in possibleAnswersMap) {
            if (!possibleAnswersMap.hasOwnProperty(answer) || !possibleAnswersMap[answer]) {
                continue;
            }

            possibleAnswers.push(answer);
        }

        set["possibleAnswers"] = possibleAnswers;
    }
}

function generateQuestion() {
    let setId = randomOf(enabledSets);
    let question = randomOf(questionSets[setId]["questions"]);
    let prohibited = [];
    prohibited.push(...question["answers"]);

    let options = [];
    for (let i = 0; i < 5; i++) {
        let opt;
        do {
            opt = randomOf(questionSets[setId]["possibleAnswers"]);
        } while(prohibited.includes(opt));

        options.push(opt);
        prohibited.push(opt);
    }

    let answerId = randomIndexOf(options);
    options[answerId] = randomOf(question["answers"]);

    currentQuestion = {
        question: question["question"],
        angleHint: question["angleHint"],
        answer: answerId,
        options: options
    };
}

/**
 * Gets the current question.
 */
function getQuestion() {
    return currentQuestion;
}

/**
 * Checks the provided answer.
 *
 * @param answerId
 */
function checkAnswer(answerId) {
    let result = (currentQuestion !== null) && (currentQuestion.answer === answerId);
    generateQuestion();
    return result;
}

/**
 * Initializes the game.
 */
function init() {
    loadQuestions();
    generateQuestion();
}

let Game = {
    init: init,
    getQuestion: getQuestion,
    checkAnswer: checkAnswer
};

export default Game;
