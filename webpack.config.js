/*
 * Cosine - a game for training basic values of trigonometric functions
 * Copyright (C) 2019 Oto Šťáva
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const path = require("path");
const webpack = require("webpack");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const HtmlWebpackIncludeAssetsPlugin = require("html-webpack-include-assets-plugin");
const PACKAGE = require("./package.json");

module.exports = {
    entry: "./src/js/main.js",
    mode: "production",
    devtool: "inline-source-map",
    devServer: {
        contentBase: "./dist",
        host: "0.0.0.0"
    },
    plugins: [
        new webpack.DefinePlugin({
            __APP__: JSON.stringify({
                name: PACKAGE.displayName,
                version: PACKAGE.version
            })
        }),
        new CopyWebpackPlugin([
            {from: "./src/css", to: "css/"},
            {from: "./src/fonts", to: "fonts/"},
            {from: "./src/res", to: "res/"},
            {from: "./node_modules/katex/dist/fonts", to: "vendor/katex/fonts/"},
            {from: "./node_modules/katex/dist/katex.css", to: "vendor/katex/"}
        ]),
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            title: "Cosine 2.0",
            template: "./src/index.html"
        }),
        new HtmlWebpackIncludeAssetsPlugin({
            assets: [
                {path: "css", glob: "*.css", globPath: "./src/css"},
                {path: "fonts", glob: "*.css", globPath: "./src/fonts"},
                "vendor/katex/katex.css"
            ],
            append: true
        })
    ],
    output: {
        filename: "main.js",
        path: path.resolve(__dirname, "dist")
    }
};